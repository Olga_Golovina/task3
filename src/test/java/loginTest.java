import io.github.bonigarcia.wdm.WebDriverManager;
import login.BasePage;
import login.LoginPage;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static login.BasePage.clickButton;
import static login.BasePage.makeScreenOnTestFail;
import static login.LoginPage.*;
import static login.LoginPage.noPasswordLabel;

public class loginTest {
    public WebDriver driver;
    public login.BasePage BasePage;
    private LoginPage newLoginPage;

    @BeforeMethod
    public void setupClass() {
        BasePage = new BasePage();
        driver = BasePage.initialize_driver();
        newLoginPage = new LoginPage(driver);
    }

    @Test(description = "Корректный логин", priority = 0)
    public void testCorrect() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем нужную страницу
        openLoginPage(driver);

        //Вводим корректные значения в поля логина и пароля
        enterField(login, "tomsmith");
        enterField(password, "SuperSecretPassword!");

        //кликаем мышью JS Click
        clickButton(driver, loginClick);


        //ассерт требуемого лейбла
        checkElementDisplayed(correctLabel);


    }

    @Test(description = "Некорректный логин", priority = 1)
    public void testNoCorrectLogin() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем нужную страницу
        openLoginPage(driver);

        //Вводим некорректное значение в поля логина
        enterField(login, "tomsmith111");

        //кликаем мышью JS Click
        clickButton(driver, loginClick);

        //ассерт требуемого лейбла
        checkElementDisplayed(noLoginLabel);

    }

    @Test(description = "Некорректный пароль", priority = 2)
    public void testNoCorrectPassword() {

        //браузер на весь экран
        maximizeBrowser(driver);

        //Открываем нужную страницу
        openLoginPage(driver);

        //Вводим в поля корректные логин и некорректный пароль

        try {
            enterField(login, "tomsmith");
            enterField(password, "11111");
        } catch (NoSuchElementException e) {
            System.err.println("Одно из полей для ввода не было найдено");
        }


/*        enterField(login, "tomsmith1");
        enterField(password, "11111");
        */



        //кликаем мышью JS Click
        clickButton(driver, loginClick);

        //ассерт требуемого лейбла
        checkElementDisplayed(noPasswordLabel);

    }

    @AfterMethod
    public void teardown(ITestResult result)  {
        makeScreenOnTestFail(result);
        if (driver != null) {
            driver.quit();

        }
    }
}